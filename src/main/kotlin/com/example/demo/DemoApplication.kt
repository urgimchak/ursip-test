package com.example.demo

import com.example.demo.utils.NumUtils.checkThatEverythingFine
import com.example.demo.utils.NumUtils.generateNum

fun main() {

	// examples from task
	var numbers = listOf(1, 2, 3)
	checkThatEverythingFine(numbers, generateNum(numbers), "004")

	numbers = listOf(1, 3)
	checkThatEverythingFine(numbers, generateNum(numbers), "002")

	numbers = listOf(2)
	checkThatEverythingFine(numbers, generateNum(numbers), "001")

	// my examples
	numbers = listOf(-1, 2, 2, 4, 10)
	checkThatEverythingFine(numbers, generateNum(numbers), "001")

	numbers = listOf(1, 2, 2, 4, 10)
	checkThatEverythingFine(numbers, generateNum(numbers), "003")

	numbers = listOf(10, 1, 2, 2, 3, 4, 5, 11)
	checkThatEverythingFine(numbers, generateNum(numbers), "006")

	numbers = listOf(1, 1, 1, 2, 1, 1, 1)
	checkThatEverythingFine(numbers, generateNum(numbers), "003")

	numbers = listOf(-2, -3, -4)
	checkThatEverythingFine(numbers, generateNum(numbers), "001")

	numbers = listOf(3, 2, 4, 1, 5, 6, 7, 8, 9, 10, 12)
	checkThatEverythingFine(numbers, generateNum(numbers), "011")

	// fail examples
	numbers = listOf(3, 4, 1)
	val result = generateNum(numbers)
	val expected = "001"
	try {
		checkThatEverythingFine(numbers, result, expected)
	} catch (exception: IllegalStateException) {
		println("Checking failed: the first missed in $numbers is not $expected, but $result")
	}
}
