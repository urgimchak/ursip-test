package com.example.demo.utils

/**
 * The class contains methods to work with numbers.
 */
object NumUtils {
	/**
	 * The template to format int to string.
	 */
	private const val THREE_DIGITS_STRING_FORMAT_TEMPLATE = "%03d"

	/**
	 * Method generates number for a project.
	 * @param numbers The existing numbers.
	 * @return The first positive missing number in sequence.
	 */
	// Реализация кажется не самой оптимальной, но и задача была не поставлена как создание самого
	// оптимального алгоритма. Думаю тут можно использовать бинарный поиск, но в любом случае коллекцию необходимо
	// заранее отсортировать
	fun generateNum(numbers: List<Int>): String {
		val uniqNumbers = numbers.distinct().sorted()
		uniqNumbers
			.forEachIndexed { index, number ->
				val expectedNumber = index + 1
				if (expectedNumber != number)
					return formatIntToThreeDigitsString(expectedNumber)
			}

		return formatIntToThreeDigitsString(uniqNumbers.size + 1)
	}

	fun checkThatEverythingFine(numbers: List<Int>, result: String, expected: String) {
		println("Checking that first positive missed in $numbers is $expected")
		check(result == expected)
	}

	/**
	 * Formats int to three digits string.
	 * @param number The number to format.
	 * @return formatted string.
	 */
	private fun formatIntToThreeDigitsString(number: Int): String =  java.lang.String.format(THREE_DIGITS_STRING_FORMAT_TEMPLATE, number)
}